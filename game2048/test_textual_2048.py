from pytest import *
import random as rd
from game2048.textual_2048 import *

def test_mock_input_return(monkeypatch):
    def mockreturn(arg):
        return rd.choice(["h","b","d","g"])
    monkeypatch.setattr('builtins.input',mockreturn)
    move=read_player_command()
    assert move in ["h","b","d","g"]
    assert move !="t"

def test_mock_input_size(monkeypatch):
    def mockreturn(arg):
        return rd.choice([k for k in range(100)])
    monkeypatch.setattr('builtins.input',mockreturn)
    taille=read_size_grid()
    assert taille in [k for k in range (100)]
    assert taille !="t"

def test_mock_input_theme(monkeypatch):
    def mockreturn(arg):
        return rd.choice([THEMES[k] for k in range (3)])
    monkeypatch.setattr('builtins.input',mockreturn)
    theme=read_size_grid()
    assert theme in [THEMES[k] for k in range (3)]
    assert theme !="t"

