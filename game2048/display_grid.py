from tkinter import *
from tkinter import font
from game2048.grid_2048 import *
from functools import partial

TILES_BG_COLOR = {0: "#9e948a", 2: "#eee4da", 4: "#ede0c8", 8: "#f1b078", \
                  16: "#eb8c52", 32: "#f67c5f", 64: "#f65e3b", \
                  128: "#edcf72", 256: "#edcc61", 512: "#edc850", \
                  1024: "#edc53f", 2048: "#edc22e", 4096: "#5eda92", \
                  8192: "#24ba63"}
TILES_FG_COLOR = {0: "#776e65", 2: "#776e65", 4: "#776e65", 8: "#f9f6f2", \
                  16: "#f9f6f2", 32: "#f9f6f2", 64: "#f9f6f2", 128: "#f9f6f2", \
                  256: "#f9f6f2", 512: "#f9f6f2", 1024: "#f9f6f2", \
                  2048: "#f9f6f2", 4096: "#f9f6f2", 8192: "#f9f6f2"}


def graphical_grid_init_2048():
    root=Tk()
    root.title('2048')          ###Titre
    window=Toplevel(root)
    window.title('2048')
    window.grid()
    background=Frame(root,height=400, width=400)
    graphical_grid=[]
    grid_game=init_game()
    grid_size=4
    for k in range(grid_size):
        for i in range(grid_size):
            frame=Frame(background,height=100, width=100,bg=TILES_BG_COLOR[grid_game[k][i]],bd=0.5,relief="solid")
            frame.grid(row=k,column=i)
    background.grid()
    root.mainloop()
    return grid_game


def display_and_update_graphical_grid(grid_game,graphical_grid,grid_size=4):               ###Permet d'update la grille game graphique ie de remplacer les frame par leur nouvelles valeurs
        for k in range(grid_size):
            for i in range(grid_size):
                if grid_game[k][i]!=0:
                    graphical_grid[k][i].config(text="{}".format(grid_game[k][i]),bg=TILES_BG_COLOR[grid_game[k][i]],bd=0.5,relief="solid")
                else:
                    graphical_grid[k][i].config(text=" ",bg=TILES_BG_COLOR[grid_game[k][i]],bd=0.5,relief="solid")


def key_pressed2(event,game_grid,graphical_grid):
        touche=event.keysym   ###On récupère la touche
        association={"Up":"h","Down":"b","Left":"g","Right":"d"}
        if touche in ["Up","Down","Left","Right"]:
            game_grid=move_grid(game_grid,association[touche])
        if is_grid_full((game_grid))==False:
            game_grid=grid_add_new_tile(game_grid)
        display_and_update_graphical_grid(game_grid,graphical_grid)

def update_label(spinbox, label, var):
    """
    Écrit 'min' ou 'max' dans label en fonction de la valeur
    du textvariable de spinbox
    """
    value = var.get()


def game_play():
    fenetre=Tk()
    fenetre.title("Choix options")
    value=DoubleVar(fenetre)
    label1=Label(fenetre,text="Choose grid size:")              ###Création du menu choix d'options
    label1.grid(row=0)
    label2=Label(fenetre,text="Choose theme:")
    label2.grid(row=2,columnspan=2)
    spinbox1 = Spinbox(fenetre, textvariable=value, from_=2, to=10, increment=1, width=20)            ###Changer et mettre un min à 2 et un max à 10 mais qui commence à 4
    spinbox1.config(command=partial(update_label, spinbox1, label1, value))
    spinbox1.grid(row=1)
    liste=Listbox(fenetre)
    liste.grid(row=4)
    liste.insert(END,"Default")
    liste.insert(END,"Chemistry")
    liste.insert(END,"Alphabet")
    def lancer_prgm(value=value,liste=liste):
        grid_size=int(value.get())          ###On récupère la valeur du spinbox
        theme=liste.curselection()[0]
        def display_and_update_graphical_grid(grid_game,graphical_grid,grid_size=grid_size):               ###Permet d'update la grille game graphique ie de remplacer les frame par leur nouvelles valeurs
            for k in range(grid_size):
                for i in range(grid_size):
                    if grid_game[k][i]!=0:
                        graphical_grid[k][i].config(text="{}".format(THEMES["{}".format(theme)][grid_game[k][i]]),bg=TILES_BG_COLOR[grid_game[k][i]],bd=0.5,relief="solid")
                    else:
                        graphical_grid[k][i].config(text=" ",bg=TILES_BG_COLOR[grid_game[k][i]],bd=0.5,relief="solid")
        root=Tk()
        root.title('2048')          ###Titre
        window=Toplevel(root)
        window.title('2048')
        window.grid()
        background=Frame(root,height=400, width=400)
        graphical_grid=[[] for k in range(grid_size)]  ###Dans chaque graphical grid, on a un label qu'on affichera en grid i,j dans background
        grid_game=init_game(grid_size)
        for k in range(grid_size):
            for i in range(grid_size):
                if grid_game[k][i]!=0:
                    graphical_grid[k].append(Label(master = background ,text="{}".format(THEMES["{}".format(theme)][grid_game[k][i]]),font=('Verdana', 20, 'bold'),bg=TILES_BG_COLOR[grid_game[k][i]],fg=TILES_FG_COLOR[grid_game[k][i]],height = 5, width = 7,bd=0.5,relief="solid"))
                else:
                    graphical_grid[k].append(Label(master = background ,text="".format(grid_game[k][i]),font=('Verdana', 20, 'bold'),bg=TILES_BG_COLOR[grid_game[k][i]],fg=TILES_FG_COLOR[grid_game[k][i]],height = 5, width = 7,bd=0.5,relief="solid"))
                graphical_grid[k][i].grid(row=k,column=i)        ###On l'affiche donc en grid ij
        def key_pressed(event,game_grid=grid_game):
            touche=event.keysym   ###On récupère la touche
            association={"Up":"h","Down":"b","Left":"g","Right":"d"}            ###On associe la touche à une direction
            old_grid=copy.deepcopy(grid_game)                                   ###On fait une copie indépendante de grid_game
            if not is_game_over(grid_game):
                if touche in ["Up","Down","Left","Right"]:
                    game_grid=move_grid(game_grid,association[touche])              ###On bouge la grille si la touche appuyée est valdie
                    if is_grid_full((game_grid))==False and game_grid!=old_grid:    ###Une nouvelle tile apparait seulement si la grille n'est pas pleine ET si la grille a changé après l'appui
                        game_grid=grid_add_new_tile(game_grid)
                display_and_update_graphical_grid(game_grid,graphical_grid)
            else:
                fenetre=Toplevel(root)
                if get_grid_tile_max(grid_game)>=2048:
                    label=Label(fenetre,text="you won")
                else:
                    label=Label(fenetre,text="you loose")
                label.grid()
        background.grid()
        root.bind("<Key>",key_pressed)                                           ###Lorsque on appuie sur une touche, fonction key_press est appelée
        root.grid()
        root.mainloop()
    frame=Frame(fenetre)
    frame.grid(row=5)
    quit_button=Button(frame, text="Quit",command=quit, width=5)
    quit_button.grid(row=5,column=0,sticky='w')
    valid_button=Button(frame, text="Play",command=lancer_prgm,width=5 )
    valid_button.grid(row=5,column=1)
    fenetre.mainloop()



print(game_play())





