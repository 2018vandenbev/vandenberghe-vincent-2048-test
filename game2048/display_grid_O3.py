from tkinter import *
from tkinter import font
from game2048.grid_2048 import *
from functools import partial

TILES_BG = [{0: "#9e948a", 2: "#eee4da", 4: "#ede0c8", 8: "#f1b078", \
                  16: "#eb8c52", 32: "#f67c5f", 64: "#f65e3b", \
                  128: "#edcf72", 256: "#edcc61", 512: "#edc850", \
                  1024: "#edc53f", 2048: "#edc22e", 4096: "#5eda92", \
                  8192: "#24ba63"},{0: "mintcream", 2: "darkturquoise", 4: "lightseagreen", 8: "plum", \
                  16: "lightskyblue", 32: "deepskyblue", 64: "dodgerblue", \
                  128: "royalblue", 256: "mediumblue", 512: "mediumpurple", \
                  1024: "rebeccapurple", 2048: "mediumvioletred", 4096: "magenta", \
                  8192: "black"}]
TILES_FG = [{0: "#776e65", 2: "#776e65", 4: "#776e65", 8: "#f9f6f2", \
                  16: "#f9f6f2", 32: "#f9f6f2", 64: "#f9f6f2", 128: "#f9f6f2", \
                  256: "#f9f6f2", 512: "#f9f6f2", 1024: "#f9f6f2", \
                  2048: "#f9f6f2", 4096: "#f9f6f2", 8192: "#f9f6f2"}, {0: "white", 2: "white", 4: "white", 8: "white", \
                  16: "white", 32: "white", 64: "white", 128: "white", \
                  256: "white", 512: "white", 1024: "white", \
                  2048: "white", 4096: "white", 8192: "white"}]

TILES_FG_COLOR_2 = {0: "white", 2: "white", 4: "white", 8: "white", \
                  16: "white", 32: "white", 64: "white", 128: "white", \
                  256: "white", 512: "white", 1024: "white", \
                  2048: "white", 4096: "white", 8192: "white"}

TILES_BG_COLOR_2 = {0: "mintcream", 2: "darkturquoise", 4: "lightseagreen", 8: "plum", \
                  16: "lightskyblue", 32: "deepskyblue", 64: "dodgerblue", \
                  128: "royalblue", 256: "mediumblue", 512: "mediumpurple", \
                  1024: "rebeccapurple", 2048: "mediumvioletred", 4096: "magenta", \
                  8192: "black"}


def graphical_grid_init_2048():
    root=Tk()
    root.title('2048')          ###Titre
    window=Toplevel(root)
    window.title('2048')
    window.grid()
    background=Frame(root,height=400, width=400)
    graphical_grid=[]
    grid_game=init_game()
    grid_size=4
    for k in range(grid_size):
        for i in range(grid_size):
            frame=Frame(background,height=100, width=100,bg=TILES_BG_COLOR[grid_game[k][i]],bd=0.5,relief="solid")
            frame.grid(row=k,column=i)
    background.grid()
    root.mainloop()
    return grid_game


def display_and_update_graphical_grid(grid_game,graphical_grid,grid_size=4):               ###Permet d'update la grille game graphique ie de remplacer les frame par leur nouvelles valeurs
        for k in range(grid_size):
            for i in range(grid_size):
                if grid_game[k][i]!=0:
                    graphical_grid[k][i].config(text="{}".format(grid_game[k][i]),bg=TILES_BG_COLOR[grid_game[k][i]],bd=0.5,relief="solid")
                else:
                    graphical_grid[k][i].config(text=" ",bg=TILES_BG_COLOR[grid_game[k][i]],bd=0.5,relief="solid")


def key_pressed2(event,game_grid,graphical_grid):
        touche=event.keysym   ###On récupère la touche
        association={"Up":"h","Down":"b","Left":"g","Right":"d"}
        if touche in ["Up","Down","Left","Right"]:
            game_grid=move_grid(game_grid,association[touche])
        if is_grid_full((game_grid))==False:
            game_grid=grid_add_new_tile(game_grid)
        display_and_update_graphical_grid(game_grid,graphical_grid)

def update_label(spinbox, label, var):
    """
    Écrit 'min' ou 'max' dans label en fonction de la valeur
    du textvariable de spinbox
    """
    value = var.get()

def score_grid(grid):
    sum=0
    list_tiles=get_all_tiles(grid)
    for tile in list_tiles:
        sum+=tile
    return sum


def game_play():
    ##Menu options
    fenetre_options=Tk()
    fenetre_options.title("Choix options")
    value=DoubleVar(fenetre_options)
    label_grid_size=Label(fenetre_options,text="Choose grid size:", font=('Helvetica', '16'),background="bisque",width=25)              ###Création du menu choix d'options
    label_grid_size.grid(row=0)
    label_theme=Label(fenetre_options,text="Choose theme:",font=('Helvetica', '16'),background="bisque",width=25)
    label_theme.grid(row=2,columnspan=2)
    label_version=Label(fenetre_options,text="Choose version:", font=('Helvetica', '16'),background="bisque",width=25)
    label_version.grid(row=5)
    spinbox_size = Spinbox(fenetre_options, textvariable=value, from_=2, to=10, increment=1, width=20)            ###Changer et mettre un min à 2 et un max à 10 mais qui commence à 4
    spinbox_size.config(command=partial(update_label, spinbox_size, label_grid_size, value))
    spinbox_size.grid(row=1)
    choices = Variable(fenetre_options, ('Default', 'Chemistry', 'Alphabet'))
    listbox_theme=Listbox(fenetre_options,listvariable=choices,exportselection=0)
    listbox_theme.grid(row=4)
    versions= Variable(fenetre_options, ('normal','green and blue version'))
    listbox_version=Listbox(fenetre_options,listvariable=versions, exportselection=0)
    listbox_version.grid(row=6)
    grid_game=0
    old_grid=0
    grid_size=0
    graphical_grid=0

    ##Programme qui se lance quand on appuie sur play
    def lancer_prgm(value=value,liste=listbox_theme, version=listbox_version):
        global grid_game
        global old_grid
        global grid_size
        global graphical_grid

        ##Récupération des valeurs des spinboxs
        grid_size=int(value.get())
        theme=liste.curselection()[0]
        version=version.curselection()[0]
        TILES_BG_COLOR=TILES_BG[version]
        TILES_FG_COLOR=TILES_FG[version]

        ##Initialisation de la nouvelle fenetre 2048
        root=Tk()
        root.title('2048')###Titre
        background=Frame(root,height=400, width=400)
        graphical_grid=[[] for k in range(grid_size)]  ###Dans chaque graphical grid, on a un label qu'on affichera en grid i,j dans background
        grid_game=init_game(grid_size)
        score_window=Frame(root,height=400, width=400)
        label_score=Label(score_window, text="Score : {}".format(score_grid(grid_game)),font=('Times', -20, 'bold'),width=25)
        label_score.pack()
        score_window.grid(row=0)
        old_grid=copy.deepcopy(grid_game)
        for k in range(grid_size):
            for i in range(grid_size):
                if grid_game[k][i]!=0:
                    graphical_grid[k].append(Label(master = background ,text="{}".format(THEMES["{}".format(theme)][grid_game[k][i]]),font=('Verdana', 20, 'bold'),bg=TILES_BG_COLOR[grid_game[k][i]],fg=TILES_FG_COLOR[grid_game[k][i]],height = 5, width = 7,bd=0.5,relief="solid"))
                else:
                    graphical_grid[k].append(Label(master = background ,text="".format(grid_game[k][i]),font=('Verdana', 20, 'bold'),bg=TILES_BG_COLOR[grid_game[k][i]],fg=TILES_FG_COLOR[grid_game[k][i]],height = 5, width = 7,bd=0.5,relief="solid"))
                graphical_grid[k][i].grid(row=k,column=i)           ###On l'affiche donc en grid ij

        ###Fonction pour mettre à jour la grille graphique ie remplacer les labels par leur nouvelle valeur
        def display_and_update_graphical_grid(grid_game,graphical_grid,grid_size=grid_size):
            for k in range(grid_size):
                for i in range(grid_size):
                    if grid_game[k][i]!=0:
                        graphical_grid[k][i].config(text="{}".format(THEMES["{}".format(theme)][grid_game[k][i]]),bg=TILES_BG_COLOR[grid_game[k][i]],bd=0.5,relief="solid")
                    else:
                        graphical_grid[k][i].config(text=" ",bg=TILES_BG_COLOR[grid_game[k][i]],bd=0.5,relief="solid")

        ##Fonction qui se lance quand on appuie sur une touche du clavier
        def key_pressed(event):
            global grid_game
            global old_grid
            old=old_grid
            touche=event.keysym   ###On récupère la touche
            association={"Up":"h","Down":"b","Left":"g","Right":"d"}            ###On associe la touche à une direction
            old_grid=copy.deepcopy(grid_game)                                   ###On fait une copie indépendante de grid_game
            if not is_game_over(grid_game):
                if touche in ["Up","Down","Left","Right"]:
                    grid_game=move_grid(grid_game,association[touche])              ###On bouge la grille si la touche appuyée est valide
                    if is_grid_full((grid_game))==False and grid_game!=old_grid:    ###Une nouvelle tile apparait seulement si la grille n'est pas pleine ET si la grille a changé après l'appui
                        grid_game=grid_add_new_tile(grid_game)
                display_and_update_graphical_grid(grid_game,graphical_grid)
                label_score.config(text="Score : {}".format(score_grid(grid_game)))
                if is_game_over(grid_game):
                    result=Label(background)
                    if get_grid_tile_max(grid_game)>=2048:
                        result.config(text="you won",font=('Times', -20, 'bold'),width=25,heigh=15,background="bisque",fg="black")
                        step_back_button.grid_forget()
                    else:
                        result.config(text="you lost",font=('Times', -20, 'bold'),width=25,heigh=15,background="bisque",fg="black")
                        restart_button.config(text="try again ?")
                        step_back_button.grid_forget()

                    result.grid(row=0,rowspan=grid_size,column=0,columnspan=grid_size)

        ##On recommence une partie lorsqu'on appuie sur le bouton restart
        def restart():
            global grid_game
            if is_game_over(grid_game):
                game_play()
            else:
                grid_game=init_game(grid_size)                  ###On réintialise la grille
                label_score.config(text="Score : {}".format(score_grid(grid_game)))           ###On réinitialise le score
                display_and_update_graphical_grid(grid_game,graphical_grid)

        ##On retourne en arrière d'un coup
        def back():
            global grid_game
            global old_grid
            display_and_update_graphical_grid(old_grid,graphical_grid)
            grid_game=copy.deepcopy(old_grid)
            label_score.config(text="Score : {}".format(score_grid(grid_game)))   ###Le score doit aussi être mis à jour

        ##Configuration des boutons
        step_back_button=Button(background,text="forget the last move",command=back)
        step_back_button.grid(row=grid_size+2,column=0,columnspan=grid_size)
        background.grid(row=1)
        root.bind("<Key>",key_pressed)                                           ###Lorsque on appuie sur une touche, fonction key_press est appelée
        restart_button=Button(background, text="Restart",command=restart)
        restart_button.grid(row=grid_size+1,column=0,columnspan=grid_size)
        root.mainloop()

    ##Configuration fenêtre principale
    frame=Frame(fenetre_options)
    frame.grid(row=7)
    quit_button=Button(frame, text="Quit",command=quit, width=5)
    quit_button.grid(row=7,column=0,sticky='w')
    valid_button=Button(frame, text="Play",command=lancer_prgm,width=5 )
    valid_button.grid(row=7,column=1)
    fenetre_options.mainloop()



print(game_play())

