from game2048.F2_Afficher_la_grille_de_jeu import THEMES
from game2048.grid_2048 import *

def read_player_command():
    move = input("Entrez votre commande (g (gauche), d (droite), h (haut), b (bas)):")
    while move not in ["g","d","h","b"]:
        move = input("S'il vous plaît, choissisez une réponse parmi : (g (gauche), d (droite), h (haut), b (bas)):")
    return move


def read_size_grid():
    size= input("Entrez la taille souhaitée pour votre grille de 2048:")
    while type(size)!=int:
        size=input("Entrez un nombre entier svp:")
    return size

def read_theme_grid():
    theme= input("Entrez le thème souhaité pour votre 2048")
    while theme not in THEMES:
        theme= input("Entrez un thème valable svp")
    return theme


def ask_and_read_grid_size():
    size=input("Quelle taille de grille souhaitez-vous? ")
    while size not in [str(k) for k in range(100)]:             ###La taille maximale ne dépasse normalement pas 100...
        input("entrez un entier svp: ")
    return size

def ask_and_read_grid_theme():
    theme=input("Quel thème souhaitez-vous? (réponse possible : 0,1 ou 2) ")
    while theme not in THEMES:
        theme=input("Entrez un thème disponible svp: ")             ###Je suppose que les seuls thèmes accessibles sont ceux du dictionnaire THEMES
    return THEMES[theme]

def game_play():
    size=ask_and_read_grid_size()
    size=int(size)
    theme=ask_and_read_grid_theme()
    grid=init_game(size)
    print(grid_to_string_with_size_and_theme(grid,theme,size))
    while is_game_over(grid)==False:
        direction=read_player_command()             ###On demande à l'utilisateur une commande
        grid=move_grid(grid,direction)              ###On bouge la grille comme demandé
        print(grid_to_string_with_size_and_theme(grid,theme,size))
        if not is_grid_full(grid):                  ###Si la grille n'est pas pleine, on rajoute une tile
            grid=grid_add_new_tile(grid)
        time.sleep(1)                               ###On lui laisse le temps de visualiser le print précédent avant d'afficher celui avec la new tile
        print(grid_to_string_with_size_and_theme(grid,theme,size))
    if get_grid_tile_max(grid)>=2048:
        return "you won"
    else:
        return "you loose"


if __name__=="__main__":                        ###Permet que le programme ne se lance que si la fenetre principale est textual (et non pas si il s'agit d'un import sur un autre fichier)
    game_play()
    exit(1)


