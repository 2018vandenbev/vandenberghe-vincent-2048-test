from random import randint
import time
from game2048.F2_Afficher_la_grille_de_jeu import *
import copy
from game2048.textual_2048 import *

def create_grid(taille=4):
    game_grid=[]
    Line=[]
    for i in range (0,taille):
        Line=[0 for i in range (taille)]
        game_grid.append(Line)
    return game_grid

def get_value_new_tile():
    choix=randint(1,10)              ###Afin que 4 soit retourner à 10% et 2 à 90%
    if choix!=10:
        return 2
    else:
        return 4

def grid_add_new_tile_at_position(grid,x,y):
    grid[x][y]=get_value_new_tile()
    return grid


def get_all_tiles(grid):
    List_tile=[]
    for ligne in grid:
        for tile in ligne:
            if type(tile)==str:                     ###Si l'élement de la liste est une chaine, c'est "" ou " " et donc la valeur attribuée est 0
                List_tile.append(0)
            else:
                List_tile.append(tile)
    return List_tile

def get_empty_tiles_positions(grid):
    list_empty_tile=[]
    for i in range (len(grid)):
        for j in range (len(grid)):
            if grid[i][j] =='' or grid[i][j] ==' ' or grid[i][j]==0:                ###On regarde les positions des tiles qui ont une valeur nulle
                list_empty_tile.append((i,j))
    return list_empty_tile

def grid_get_value(grid,x,y):
    if type(grid[x][y])==int and grid[x][y]!=0:
        return grid[x][y]
    return 0

def get_new_position(grid):
    list_empty_tiles_positions=get_empty_tiles_positions(grid)      ###Liste des positions possibles
    choix=randint(0,len(list_empty_tiles_positions)-1)
    return list_empty_tiles_positions[choix]   ###on retourne une position valable au hasard parmis celles possibles


def grid_add_new_tile(grid):
    new_position_X,new_position_Y=get_new_position(grid)
    grid_add_new_tile_at_position(grid,new_position_X,new_position_Y)
    return grid

def init_game(taille=4):
    grid=create_grid(taille)
    grid=grid_add_new_tile(grid)
    grid=grid_add_new_tile(grid)            ###On ajoute deux tiles aléatoirement sur la grille
    return grid


### Fonctionnalité 4 (faire les tests pour la fonctionnalité 3


def move_row_left(list):
    list_tile=[]
    for i in range (len(list)):
        if list[i]!=0:
            list_tile.append(list[i])           ###On crée la liste de toutes les tiles non nulles de 'list'
    for k in range(len(list_tile)-1):
        if list_tile[k]==list_tile[k+1]:        ###Si deux tiles consécutives sont égales, on les fusionne ET on supprime la 2e. On rajoute alors un 0 à la fin de la liste
            list_tile[k]*=2
            list_tile.pop(k+1)
            list_tile.append(0)
    return list_tile+[0 for k in range (len(list)-len(list_tile))]          ###On complète la liste par des zéros


def move_row_right(list):
    for k in range (len(list)//2):              #On utilise l'algorithme précédent pour coder le move right
        list[k],list[-k-1]=list[-k-1],list[k]
    list=move_row_left(list)
    for k in range (len(list)//2):
        list[k],list[-k-1]=list[-k-1],list[k]
    return list

def move_row_up_grid(grid):
    for k in range (len(grid)):
        list=[]
        for j in range (len(grid)):  ### On prend le k élément de chaque ligne, qu'on insère dans une liste. On utilise la fonction move left sur cette liste et on réinsère les éléments dans le tableau à leur place
            list.append(grid[j][k])
        list=move_row_left(list)
        for j in range (len(grid)):
            grid[j][k]=list[j]
    return grid

def move_row_down_grid(grid):
    for k in range (len(grid)):
        list=[]
        for j in range (len(grid)):  ###même principe que pour move_up sauf qu'on utilise move right cette fois -ci
            list.append(grid[j][k])
        list=move_row_right(list)
        for j in range (len(grid)):
            grid[j][k]=list[j]
    return grid

def move_row_right_grid(grid):
    for k in range(len(grid)):
        grid[k]=move_row_right(grid[k])             ###On applique move_right(list) à tous les élements de grid
    return grid

def move_row_left_grid(grid):                       ###On applique move_left(list) à tous les élements de grid
    for k in range(len(grid)):
        grid[k]=move_row_left(grid[k])
    return grid

def move_grid(grid,d):
    association={"left":"g","right":"d","up":"h","down":"b"}                        ### on associe left à g, right à d etc.
    while d not in ["left","right","up","down"] and d not in ["g","d","h","b"]:     ### deux réponses possibles acceptées
        d=input("svp entrez right,left,up ou down")
    if d=="right" or d==association["right"]:
        return move_row_right_grid(grid)
    elif d=="left" or d==association["left"]:
        return move_row_left_grid(grid)
    elif d=="up" or d==association["up"]:
        return move_row_up_grid(grid)
    elif d=="down" or d==association["down"]:
        return move_row_down_grid(grid)



def is_grid_full(grid):
    if len(get_empty_tiles_positions(grid))==0:  ###ie si la liste des positions où on a une tile qui vaut 0 est vide, alors on renvoie vrai
        return True
    return False

def move_possible(grid):
    L=[]
    new_grid=copy.deepcopy(grid)     ###On fait une deepcopy afin que les listes de liste ne soient pas liées
    for d in ["left","right","up","down"]:
        if grid==move_grid(new_grid,d):        ###Si la liste que l'on a bougé selon la direction d est identique à celle initiale, c'est que le mouvement est impossible
            L.append(False)
        else:
            L.append(True)
    return L



def is_game_over(grid):
    if move_possible(grid)==[False,False,False,False] and is_grid_full(grid)==True:             ###La condition move_possible est normalement nécessaire et suffisante
        return True
    return False

def get_grid_tile_max(grid):                ###retourne le maximum des tiles de la grille : utile pour savoir si l'utilisatuer a gagné ou non
    list=get_all_tiles(grid)
    return max(list)


def random_play(taille=4):
    grid=init_game(taille)          ###Initialisation
    print("grille initiale:")
    print(grid_to_string_with_size(grid,taille))
    association={1:"left",2:"right",3:"up",4:"down"}
    while is_game_over(grid)==False:        ###Tant que le jeu n'est pas fini
        direction=randint(1,4)              ###Direction au hasard
        print(association[direction])
        grid=move_grid(grid,association[direction])  ###On bouge la grille à l'étape suivante(ce qui est possible car isgameover==False
        print(grid_to_string(grid,taille))
        if not is_grid_full(grid):          ### Si la grille est pleine, on ajoute pas de tile
            grid=grid_add_new_tile(grid)
        print(grid_to_string_with_size(grid,taille))
    if get_grid_tile_max(grid)>=2048:
        return "you won"
    else:
        return "you loose"






