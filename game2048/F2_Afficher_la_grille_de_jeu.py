

def grid_to_string(grid,size):
    a=""""""
    for i in range (size):
        a+=" ==="                                                       ### On place la première rangée de =
    for j in range (size):
        a+=str("\n")
        a+="|"
        for k in range (size):
            if grid[j][k]!=0 and grid[j][k]!= '' and grid[j][k]!= ' ':   ###Si la grille n'est pas composé d'un élément nulle, on doit entrer la valeur dans la chaine entre les |
                a+=" {} ".format(grid[j][k])
            else:                                                        ###Sinon, on met uniquement des espaces entre les |
                a+="   "
            a+="|"
        a=a+str("\n")
        a+=""
        for i in range (size):
            a+=" ==="                                                      ###On place la rangée inférieure de =
    return a

def longueur(nombre):
    i=1
    while nombre//10**i>0:
        i+=1
    return i

def long_value(grid):
    list_tile=[]
    for ligne in grid:
        for tile in ligne:
            if type(tile)==str:
                list_tile.append(0)
            else:
                list_tile.append(tile)
    longest=max(list_tile)
    return longueur(longest)

def grid_to_string_with_size(grid, size):
    largeur_case=long_value(grid)
    str_grid= """ """
    for i in range (size):
        for nombre_egal in range (largeur_case+2):     ###Le nombre de = dépend de la "longueur" maximale du nombre
            str_grid+= "="
        str_grid+= " "
    for j in range (size):                             ### processus itératif qui permet de créer chaque ligne de la châine : retour à la ligne puis mise en place des différents | nombre |
        str_grid+=str("\n")
        str_grid+= "|"
        for k in range (size):
            if grid[j][k]!=0 and grid[j][k]!= '' and grid[j][k]!= ' ':  ###Si ce n'est pas un 0, on doit s'interesser à la valeur du nombre
                longueurchaine=longueur(grid[j][k])
                espace_pre_number=((largeur_case+2)-longueurchaine)//2  ###On rajoute des espaces entre le | et le nombre, espace qui laisse tout de même de la place au nombre dans la case
                for l in range (espace_pre_number):
                    str_grid+= " "
                str_grid+= "{}".format(grid[j][k])                      ###On rajoute la valeur du nombre
                for l in range (espace_pre_number):                     ### On rajoute des espaces après le nombre
                    str_grid+= " "
                if espace_pre_number+longueurchaine!=largeur_case+2-espace_pre_number:    ###Permet de gérer le cas où il reste un espace à mettre pour que les cases soient de tailles égales
                    str_grid+= " "
            else:
                for l in range (largeur_case+2):                        ###Si c'est un 0, on rajoute juste des espaces entre les |
                    str_grid+= " "
            str_grid+= "|"                                              ###On ferme la ligne avec un |
        str_grid= str_grid + str("\n")
        str_grid+= " "
        for i in range (size):                                          ###On rajoute la rangée inférieur de =
            for k in range (largeur_case+2):                            ###la rangée inférieur de = dépend de la taille de la case
                str_grid+= "="
            str_grid+= " "
    return str_grid



THEMES = {"0": {"name": "Default", 0: "", 2: "2", 4: "4", 8: "8", 16: "16", 32: "32", 64: "64", 128: "128", 256: "256", 512: "512", 1024: "1024", 2048: "2048", 4096: "4096", 8192: "8192"}, "1": {"name": "Chemistry", 0: "", 2: "H", 4: "He", 8: "Li", 16: "Be", 32: "B", 64: "C", 128: "N", 256: "O", 512: "F", 1024: "Ne", 2048: "Na", 4096: "Mg", 8192: "Al"}, "2": {"name": "Alphabet", 0: "", 2: "A", 4: "B", 8: "C", 16: "D", 32: "E", 64: "F", 128: "G", 256: "H", 512: "I", 1024: "J", 2048: "K", 4096: "L", 8192: "M"}}

def long_value_with_theme(grid,dico):
    list_str=[]
    for line in grid:
        for tile in line:
            list_str.append(dico[tile])
    return max_longueur_str(list_str)

def max_longueur_str(list):
    max=len(list[0])
    for i in list:
        if len(i)>max:
            max=len(i)
    return max


def grid_to_string_with_size_and_theme(grid,dico,size):
    largeur_case=long_value_with_theme(grid,dico)                           #Cette fois, la largeur de la case dépend de la taille de la plus grande valeur du dico (parmis les valeurs associées aux nombres de la grille), et non pas de la plus grande valeur de la grille
    str_grid= """ """
    for i in range (size):
        for nombre_egal in range (largeur_case+2):
            str_grid+= "="
        str_grid+= " "
    for j in range (size):
        str_grid+=str("\n")
        str_grid+= "|"
        for k in range (size):
            if dico[grid[j][k]]!=0 and dico[grid[j][k]]!= '' and dico[grid[j][k]]!= ' ':        #Cette fois, c'est la valeur du dico qui est importante
                longueurchaine=len(dico[grid[j][k]])                                            #de même pour le nombre d'espace qui se base sur la valeur du dico
                espace_pre_number=((largeur_case+2)-longueurchaine)//2  ###On rajoute des espaces entre le | et la valeur, espace qui laisse tout de même de la place à la valeur dans la case
                for l in range (espace_pre_number):
                    str_grid+= " "
                str_grid+= "{}".format(dico[grid[j][k]])                ###On rajoute la valeur du dico
                for l in range (espace_pre_number):     ### On rajoute des espaces après la valeur
                    str_grid+= " "
                if espace_pre_number+longueurchaine!=largeur_case+2-espace_pre_number:
                    str_grid+= " "
            else:
                for l in range (largeur_case+2):
                    str_grid+= " "
            str_grid+= "|"
        str_grid= str_grid + str("\n")
        str_grid+= " "
        for i in range (size):
            for k in range (largeur_case+2):
                str_grid+= "="
            str_grid+= " "
    return str_grid
