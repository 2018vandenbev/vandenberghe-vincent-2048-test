from game2048.F2_Afficher_la_grille_de_jeu import *

def test_grid_to_string():
    a =""" === === === ===
|   |   |   |   |
 === === === ===
|   |   |   |   |
 === === === ===
|   |   |   |   |
 === === === ===
| 2 |   |   | 2 |
 === === === ==="""
    assert grid_to_string([[' ', ' ', ' ', ' '], [' ', ' ', ' ', ' '], [' ', ' ', ' ', ' '], [2, ' ', ' ', 2]],4)==a


def test_long_value():
    assert long_value([[' ', ' ', 32, ' '], [' ', ' ',10010, ' '], [' ', 12, ' ', ' '], [2, ' ', ' ', 2]])==5
    assert long_value([[' ', ' ', 32, ' '], [' ', ' ',1, ' '], [' ', 12, ' ', ' '], [2, ' ', ' ', 2]])==2

def test_grid_to_string_with_seize():
    a =""" ==== ==== ==== ==== 
|    |    |    |    |
 ==== ==== ==== ==== 
|    |    |    | 12 |
 ==== ==== ==== ==== 
|    |    |    |    |
 ==== ==== ==== ==== 
| 25 |    |    | 2  |
 ==== ==== ==== ==== """
    assert grid_to_string_with_size([[' ', ' ', '', ' '], [' ', ' ',' ', 12], [' ', '', ' ', ' '], [25, ' ', ' ', 2]],4)==a

def test_long_value_with_theme():
    grid =[[2048, 16, 32, 0], [0, 4, 0, 2], [0, 0, 0, 32], [512, 1024, 0, 2]]
    assert long_value_with_theme(grid,THEMES["0"]) == 4
    assert long_value_with_theme(grid,THEMES["1"]) == 2
    assert long_value_with_theme(grid,THEMES["2"]) == 1
    grid = [[16, 4, 8, 2], [2, 4, 2, 128], [4, 512, 32, 4096], [1024, 2048, 512, 2]]
    assert long_value_with_theme(grid,THEMES["0"]) == 4
    assert long_value_with_theme(grid,THEMES["1"]) == 2
    assert long_value_with_theme(grid,THEMES["2"]) == 1

print(test_long_value_with_theme())

def test_grid_to_string_with_size_and_theme():
    grid=[[16, 4, 8, 2], [2, 4, 2, 128], [4, 512, 32, 64], [1024, 2048, 512, 2]]
    a=""" ==== ==== ==== ==== 
| Be | He | Li | H  |
 ==== ==== ==== ==== 
| H  | He | H  | N  |
 ==== ==== ==== ==== 
| He | F  | B  | C  |
 ==== ==== ==== ==== 
| Ne | Na | F  | H  |
 ==== ==== ==== ==== """
    assert grid_to_string_with_size_and_theme(grid,THEMES["1"],4)== a

print(test_grid_to_string_with_size_and_theme())


