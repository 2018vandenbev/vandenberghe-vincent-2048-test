"""from tkinter import *           ###On importe tinker

###CREATION FENETRE
fenetre=Tk()           ###On crée un objet de classe Tk

###AFFICHAGE TEXT
champ_label=Label(fenetre,text="Salut !!")          ###On crée un label, ie un objet graphique affichant du texte
champ_label.pack()   ###On utilise la méthode pack sur notre label, qui permet de positionner un objet sur la fenetre (et donc de l'afficher)
champ_label.config(text="Hello World !")    ###On peut changer le text de cette manière
print(champ_label["text"]) ###Permet d'afficher le text sur la console python

###BOUTON QUITTER
bouton_quitter = Button(fenetre, text="Quitter", command=fenetre.quit)      ###On crée un bouton quitter qui opère la command fenetre.quit ie fermer la fenetre
bouton_quitter.pack()

###LIGNE DE SAISIE
var_texte = StringVar()             ###Variabla qui va contenir notre texte sur la fenêtre
ligne_texte = Entry(fenetre, textvariable=var_texte, width=30)      ###Config de l'entrée
ligne_texte.pack(side="bottom",fill=X)          ###Affichage
"""
                                      ###Mainloop qui permet d'ouvrir notre fenêtre racine et ne s'arrete que lorsque qu'on ferme la fenetre

###N'oubliez pas que, pour qu'un widget apparaisse, il faut :
# qu'il prenne, en premier paramètre du constructeur, la fenêtre principale ;
#qu'il fasse appel à la méthode pack.


"""CASE A COCHER"""

"""BOUTONS RADIO"""

"""LISTES DEROULANTES"""

"""ORGANISATION DES WIDGETS DANS LA FENETRE"""


"""import tkinter as tk


def write_text():
    print("Hello CentraleSupelec")

root = tk.Tk()
frame = tk.Frame(root)          ###Sorte de sous fenetre qui peut contenir des widgets, ie widgets qui peut contenir des widgets
frame.pack()                    ###On affiche cette sous fenetre

button = tk.Button(frame,
                   text="QUIT",
                   activebackground = "blue",
                   fg="red",
                   command=quit)                ###On insère un boutton  rouge quit
button.pack(side=tk.TOP)                       ###On l'affiche en haut
slogan = tk.Button(frame,
                   fg="blue",
                   text="Hello",                 ###Pareil
                   command=write_text)           ###command=print(..) permet d'afficher la commande quand on appuie sur le boutton
slogan.pack(side=tk.BOTTOM)                       ###On l'affiche en bas

root.mainloop()"""


"""from tkinter import Tk, StringVar, Label, Entry, Button
from functools import partial

def update_label(label, stringvar):
    
    ###Met à jour le texte d'un label en utilisant une StringVar.
    
    text = stringvar.get()
    label.config(text=text)
    stringvar.set('merci')        

root = Tk()                     ###On ouvre une fenêtre
text = StringVar(root)
label = Label(root, text='Your name')       ###On entre un affichage
entry_name = Entry(root, textvariable=text)        ###On entre une entry pour l'utilisateur
button = Button(root, text='clic',
                command=partial(update_label, label, text))     ###On entre un boutton dans root, avec pour text "clic" et qui utilise update_label
                                                                ###Partial est indispensable pour utiliser une fonction qui est externe
label.grid(column=0, row=0)             ###On décide de la position de label, entry_name et button en grille
entry_name.grid(column=0, row=1)
button.grid(column=0, row=2)
root.mainloop()"""

"""from tkinter import Tk, Label, Frame

root = Tk()
f1 = Frame(root, bd=1, relief='solid')
Label(f1, text='je suis dans F1').grid(row=0, column=0)
Label(f1, text='moi aussi dans F1').grid(row=0, column=1)

f1.grid(row=0, column=0)         ###La position dans root de f1 est : 1e ligne, 1e position (si on oublie cette ligne ca n'affiche rien)
Label(root, text='je suis dans root').grid(row=1, column=0)
Label(root, text='moi aussi dans root').grid(row=2, column=0)

root.mainloop()"""

"""from tkinter import *
from pprint import pformat

def print_bonjour(i):
    label.config(text="Hello")

root = Tk()
frame = Frame(root, bg='white', height=100, width=400)   ###Height et width donnent la taille de la grille
entry = Entry(root)
label = Label(root)

frame.grid(row=0, column=0)         ###On place le fram
entry.grid(row=1, column=0, sticky='ew')            ###On place entry
label.grid(row=2, column=0)                         ###On place le labek

frame.bind('<ButtonPress>', print_bonjour)                 ###Permet de faire un évènement : quand on appuie dans la fenetre, la fonction print bonjour est réalisé
entry.bind('<KeyPress>', print_bonjour)                    ###Permet de print bonjour quand on utilise le clavier
root.mainloop()"""
"""from tkinter import Tk, DoubleVar, Label, Spinbox
from functools import partial

def update_label(spinbox, label, var):
    #Écrit min ou max dans label en fonction de la valeur
    #du textvariable de spinbox
    value = var.get()
    if value == spinbox.cget('from'):
        label.config(text='Min')
    elif value == spinbox.cget('to'):
        label.config(text='Max')

root = Tk()
value = DoubleVar(root)
label = Label(text=4)
spinbox = Spinbox(root, textvariable=value, from_=4, to=8, increment=0.5)
spinbox.config(command=partial(update_label, spinbox, label, value))

spinbox.grid(row=0, column=0)
label.grid(row=1, column=0)

root.mainloop()"""


from tkinter import *
from functools import partial

def show_selection(label, choices, listbox):
    choices = choices.get()
    text = ""
    for index in listbox.curselection():
        text += choices[index] + " "

    label.config(text=text)

root = Tk()
choices = Variable(root, ('P2R-866', 'PJ2-445', 'P3X-974'))
listbox = Listbox(root, listvariable=choices, selectmode="multiple")
listbox.insert('end', 'P3X-972', 'P3X-888')
label = Label(root, text='')
button = Button(root, text='Ok', command=partial(show_selection, label, choices, listbox))

listbox.grid(row=0, column=0)
button.grid(row=1, column=0)
label.grid(row=2, column=0)
root.mainloop()
